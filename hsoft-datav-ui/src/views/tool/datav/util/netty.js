import store from '@/store'
import Vue from 'vue';
let vue = new Vue();

window.CHAT = {
    /**
     * netty服务后端发布的url地址
     */
    nettyServerUrl: process.env.VUE_APP_WEBSOCKET,
    socket: null,
    init: function () {
        if (window.WebSocket) {
            CHAT.socket = new WebSocket(CHAT.nettyServerUrl);
            CHAT.socket.onopen = function () {
                console.log("连接建立成功...");
            }, CHAT.socket.onclose = function () {
                console.log("连接关闭...");
            }, CHAT.socket.onerror = function () {
                console.log("发生错误...");
            }, CHAT.socket.onmessage = function (e) {
                // console.log("OvO", e);
                console.log("接受到消息：" + e.data);
                // 获取message和ids的值
                var mes = JSON.parse(e.data);
                if (mes.type == 1) { // 分享
                    //alert("OvO");
                    console.log(store.getters.name); //获取当前登录人
                    var message = mes.message;
                    var receiverArr = mes.receiverIds.split(',');
                    //判断分享的人是不是自己
                    if (receiverArr.indexOf(store.getters.name) > -1) { 
                        vue.$notify.info({
                            //confirmButtonText: '确定',
                            title: '来自<' + mes.sender + '>的分享,请刷新查看。',
                            message: message,
                            duration: 0
                        });
                    }
                } 
                // 工单提醒
                else if (mes.type == 2) { 
                    var message = mes.message;
                    var receiverArr = mes.receiverIds.split(',');
                    //判断分享的人是不是自己
                    if (receiverArr.indexOf(store.getters.name) > -1) { 
                        vue.$notify.info({
                            //confirmButtonText: '确定',
                            title: '您有新的工单消息,请刷新查看。',
                            message: message,
                            duration: 0
                        });
                    }
                }
                
            }
        } else {
            alert("浏览器不支持websocket协议...");
        }
    },
    chat: function () {
        var msg = document.getElementById("msgContent");
        CHAT.socket.send(msg.value);
    }
};

CHAT.init();

export default CHAT;