# 轮播图

## 一、配置

- 图层名称：组件的名称。（建议设置，方便后期组件管理）

- 轮播效果：图片翻页效果，可选位移、褪色、轻弹、立方体、覆盖流，例如设置为立方体，如下图；

  <viewer>
  <img src="../image/rotation-1.png"width="60%">
  </viewer>

- 轮播速度：图片翻页速度，可选一阶（2.5s）、二阶（5s）、三阶（7.5s）、四阶（10s）；

- 轮播方向：图片翻页方向，可选横向或纵向；

- 是否关闭分页器开关：控制图片翻页分页器是否显示，设置关闭后点击图片分页器隐藏；

- 分页样式：图片显示分页的样式，可选圆点、分式、进度条，例如设置为圆点，如下图；

  <viewer>
  <img src="../image/rotation-2.png"width="60%">
  </viewer>

- 图库：点击图库按钮在打开的图库页面中选择轮播图片,鼠标点击图片即可选中一张图片，可选择多张图片，
  再次点击即取消选中，点击确认后即可生成轮播图；

  <viewer>
  <img src="../image/rotation-3.png"width="60%">
  </viewer>

- 载入动画：组件的载入动画效果。分别为：弹跳、渐隐渐显、向右滑入、向左滑入、放缩、旋转、滚动。
