import request from '@/utils/request'

// 查询未过期但以不在redis中的token列表
export function listShareToken(query) {
  return request({
    url: '/datav/token/list',
    method: 'get',
    params: query
  })
}
// 恢复token
export function recoverToken(data) {
  return request({
    url: '/datav/token/recover',
    method: 'post',
    data: data
  })
}