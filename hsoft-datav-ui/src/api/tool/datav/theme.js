import request from '@/utils/request'

// 查询数据大屏自定义主题配置列表
export function listTheme(query) {
  return request({
    url: '/datav/theme/list',
    method: 'get',
    params: query
  })
}

// 查询数据大屏自定义主题配置详细
export function getTheme(themeId) {
  return request({
    url: '/datav/theme/' + themeId,
    method: 'get'
  })
}

// 新增数据大屏自定义主题配置
export function addTheme(data) {
  return request({
    url: '/datav/theme',
    method: 'post',
    data: data
  })
}

// 修改数据大屏自定义主题配置
export function updateTheme(data) {
  return request({
    url: '/datav/theme',
    method: 'put',
    data: data
  })
}

// 删除数据大屏自定义主题配置
export function delTheme(themeId) {
  return request({
    url: '/datav/theme/' + themeId,
    method: 'delete'
  })
}

// 导出数据大屏自定义主题配置
export function exportTheme(query) {
  return request({
    url: '/datav/theme/export',
    method: 'get',
    params: query
  })
}